from tkinter import*
from time import*


class Splash_Screen(object):
	"""Splash_Screen: Es la ventana de Bienvenida"""
	def __init__(self):
		
		self.root=Tk()
		self.tipo_letra="Arial black"
		self.root.geometry('400x200+450+160')
		self.color="white"
		self.root.config(bg=self.color)
		self.root.overrideredirect(1)
		self.fondo = Label(self.root,text="Creador\n Fredys Marquez Duque",bg=self.color,font=(self.tipo_letra,8)).place(x=240,y=165)
		self.root.attributes("-alpha",0.9)
		self.mensaje = ["AUTOMATA DE PILA","ALIP ED ATAMOTUA","AUTOMATA DE PILA","ALIP ED ATAMOTUA","AUTOMATA DE PILA","ALIP ED ATAMOTUA","AUTOMATA DE PILA","ALIP ED ATAMOTUA","AUTOMATA DE PILA","ALIP ED ATAMOTUA"]    
		self.colores = ["brown","orange","dark turquoise","maroon","red","yellow","green","purple","violet","black"]
		
		for i  in range(int(len(self.mensaje))):
  			 
  		 	 self.fondo1 = Label(self.root,text=self.mensaje[i],bg=self.color,fg=self.colores[i],font=(self.tipo_letra,16)).place(x=80,y=90)
  		 	 self.root.update()	
  		 	 sleep(0.8)
          
		self.root.after(2200,self.root.destroy)
		self.root.mainloop()

