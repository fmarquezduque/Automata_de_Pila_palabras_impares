from tkinter import*
from time import*


class Creditos(object):
	"""Creditos: Es la ventana de grupo desarrollador del software"""
	def __init__(self):
		
		self.root=Tk()
		self.tipo_letra="Arial black"
		self.root.geometry('500x350+450+160')
		self.color="black"
		self.root.config(bg=self.color)
		self.root.overrideredirect(1)
		self.root.attributes("-alpha",1)
	    
		self.colores = "black"
		 
		titulo = Label(self.root, text="Compiladores 2019-1\n\n\n\nPresentado a\n\nDaniel Gonzalez\n\n\n\n\n9/05/2019",font=(self.tipo_letra,14),fg="white",bg=self.color).pack()  
		self.root.after(7200,self.root.destroy)
		self.root.mainloop()

